<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Ovaj model sluzi iskljucivo za komunikaciju sa tabelom customer u bazi
 *
 * @author marko.vekaric
 */
class CustomerModel extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
 /**
 * Funkcija updateCustomer menja podatke customer tablice sa odredjenim idjem - konkretno menja im ime i prezime.
 * @return void
 * @author marko.vekaric
 */
    public function updateCustomer($user, $data){
        $this->db->set('Name', $data["name"]);
        $this->db->set('Surname', $data["surname"]);
        $this->db->where('IDCustomer',$user->IDUser)->update('customer');
    }
    
 /**
 * Funkcija insertCustomer ubacuje u tabelu customer novog korisnika
 * @return void
 * @author marko.vekaric
 */
    public function insertCustomer($data){
        $idUser = $this->db->from('user')->where("User_name", $data['username'])->get()->row()->IDUser;
        $this->db->flush_cache();
        $this->db->set('IDCustomer', $idUser);
        $this->db->set('Name', $data['name']);
        $this->db->set('Surname', $data['surname']);
        $this->db->insert('customer');
    }
   
    
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Comment Model je model zaduzen za komunikaciju sa tabelom comment u bazi
 *
 * @author marko.vekaric
 */
class CommentModel extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
 /**
 * 
 *  Ova funkcija dohvata sve komentare i ocene koji su vezani za odredjenog hosta i vraca rezultat u vidu tabele kroz koju ce se posle iterirati
 * 
 * @return table
 * @author marko.vekaric
 */
    public function getCommentsForHost($host){
        $this->db->select('*');
        $this->db->from('comment');
        $this->db->join('rate_restaurant', 'comment.IDComment = rate_restaurant.IDRate_Restaurant');
        $this->db->join('customer', 'rate_restaurant.IDCustomer = customer.IDCustomer','left');
        $this->db->where('IDHost_Restaurant', $host->IDHost_Restaurant);
        return $this->db->get()->result();
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Ovaj model sluzi za komunikaciju ubacivanje i brisanje ocena restorana od strane korisnika
 *
 * @author marko.vekaric
 */
class RateRestaurantModel extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	
 /**
 * Funkcija deleteRate Sluzi za brisanje ocene u vezi restorana za odredjeni restoran i odredjenog klijenta odnosno njihove idjeve
 * @return void
 * @author MARKO
 */
    public function deleteRate($idRatedRestaurant,$idCustomer){
        $this->db->delete('rate_restaurant', array('IDHost_Restaurant' => $idRatedRestaurant,'IDCustomer'=>$idCustomer));
    }
	
 /**
 * Funkcija insertRate je funkcija za klasicno insertovanje ocene restorana u bazu i pri tome insertovanje i komentara ukoliko on postoji.
  * Koristi se zajedno sa funkcijom deleteRate da bi se obezbedila jedinstvenost komentara za odredjenu osobu odredjenog restorana.
 * @return void
 * @author MARKO
 */
    public function insertRate($idRestaurant,$idCustomer,$ocena,$comment){
        $data = array(
        'IDHost_Restaurant' => $idRestaurant,
        'IDCustomer' => $idCustomer,
        'Rate' => $ocena,
        );
        $this->db->insert('rate_restaurant', $data);
        if($comment != null){
            $rate = $this->db->from('Rate_restaurant')->where('IDHost_Restaurant',$idRestaurant)->where('IDCustomer',$idCustomer)->get()->row();
            $idrate = $rate->IDRate_Restaurant;
            $this->db->insert('Comment',['Text' => $comment, 'IDComment' => $idrate]);
        }    
    }
}

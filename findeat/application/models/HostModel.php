<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HostModel
 *
 * @author filip.tanic
 */
class HostModel extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
 /**
 * Dohvatanje ugostitelja(host) na osnovu prosledjenog korisnika
 *
 * @return host
 */
    public function getHost($user){
        $host = $this->db->from('host_restaurant')->where('idhost_restaurant',$user->IDUser)->get()->row();
        return $host;
    }
    
 /**
 * Dohvatanje ugostitelja(host) na osnovu prosledjenog IDHost-a
 *
 * @return host
 */
    public function getHostOverId($idhost){
        $host = $this->db->from('host_restaurant')->where('idhost_restaurant',$idhost)->get()->row();
        return $host;
    }
    
 /**
 * Dohvatanje adresa jednog ugostitelja(host)
 *
 * @return addresses[]
 */
    public function getAddresses($host){
        return $this->db->from('location')->where('idhost_restaurant',$host->IDHost_Restaurant)->get()->result();
    }
    
 /**
 * Promena podataka ugostitelja(host) na osnovu prosledjenih podataka
 *
 * @return void
 */
    public function updateHost($user, $data){

        if($data["newimage"] != null)
            $this->db->query("update host_restaurant set Image ='".$data['newimage']."' where IDHost_restaurant= '".$user->IDUser."'");
        //$this->db->set('Image', $data["newimage"]);
        if($data["newmenu"] != null)
            $this->db->query("update host_restaurant set Menu ='".$data['newmenu']."' where IDHost_restaurant= '".$user->IDUser."'");
        // $this->db->set('Menu', $data["newmenu"]);
        $this->db->set('Name', $data["name"]);
        $this->db->set('Description', $data["description"]);
		$this->db->where('IDHost_restaurant',$user->IDUser)->update('host_restaurant');
        if($data["newaddress"] != null){
            $addresses = $this->HostModel->getAddresses($this->HostModel->getHost($_SESSION['user']));
            $b = false;
            $adr = "";
           foreach ($addresses as $address){
                $adr = $adr.$address->Address." ";
            }
           //redirect('Home_restaurant/greska/'.$data["newaddress"]." | ".$adr);

            foreach ($addresses as $address) {

                if($address->Address == $data["newaddress"]){

                    $b = true;
                    break;
                }
            }
            if(!$b){ //TODO
                //redirect('Home_restaurant/greska');
                $this->db->flush_cache();
                $this->db->set('Address', $data["newaddress"]);
                $this->db->set('IDHost_restaurant', $user->IDUser);
                $this->db->insert('location');
            }
        }
        $address = $this->db->from('location')->where('IDHost_Restaurant',$user->IDUser)->order_by('IDLocation','desc')->get()->row();
        if($data['newtelephone'] != null){
            $this->db->set('Number', $data["newtelephone"]);
            $this->db->set('IDLocation', $address->IDLocation);
            $this->db->insert('telephone');
        }
    }
    
 /**
 * Ubacivanje novog ugostitelja(host) na osnovu prosledjenih podataka
 *
 * @return void
 */
    public function insertHost($data){
        $idUser = $this->db->from('user')->where("User_name", $data['username'])->get()->row()->IDUser;
        $this->db->flush_cache();
        $this->db->set('IDHost_restaurant', $idUser);
        $this->db->set('Name', $data['name']);
        $this->db->insert('host_restaurant');
		
	if($data["picture"] != null)
            $this->db->query("update host_restaurant set Image ='".$data['picture']."' where IDHost_restaurant= '".$idUser."'");
        if($data["menu"] != null)
            $this->db->query("update host_restaurant set Menu ='".$data['menu']."' where IDHost_restaurant= '".$idUser."'");
		
        $this->db->flush_cache();
        if($data['address'] != null){
            $this->db->set('Address', $data["address"]);
            $this->db->set('IDHost_restaurant', $idUser);
            $this->db->insert('location');
        }
        $address = $this->db->from('location')->order_by('IDLocation','desc')->get()->row();
        if($data['telephone'] != null){
            $this->db->set('Number', $data["telephone"]);
            $this->db->set('IDLocation', $address->IDLocation);
            $this->db->insert('telephone');
        }
    }
    
 /**
 * Dohvatanje ugostitelja(host) koji je vlasnik prosledjenog jela(meal)
 *
 * @return host_restaurant
 */
    public function getRestaurant($meal){
        $this->db->select('host_restaurant.Name, host_restaurant.IDHost_Restaurant');
        $this->db->from('meal');
        $this->db->join('host_restaurant', 'meal.IDHost_Restaurant = host_restaurant.IDHost_Restaurant');
        $this->db->where('IDMeal', $meal->IDMeal);
        return $this->db->get()->row();
    }
	
}
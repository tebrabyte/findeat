<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Ovaj model sluzi za komunikaciju sa tabelom koja se odnosi na ocene restorana
 *
 * @author MARKO
 */
class RateMealModel extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
 /**
 * Funkcija deleteRate brise komentar za odredjeno jelo i odredjenog customera to jest za njihove prosledjene idjeve.
 * @return void
 * @author MARKO
 */
    public function deleteRate($idRatedMeal,$idCustomer){
        $this->db->delete('rate_meal', array('IDMeal' => $idRatedMeal,'IDCustomer'=>$idCustomer));
    }
	
 /**
 * Funkcija insertRate sluzi sa obicno ubacivanje ocene u tabelu ocena jela.
 * Ova i funkcija iznad koristice se u paru da bi se obezbedila jedinstvenost komentara jela za odredjenog korisnika na nivou aplikacije. (obezbedjeno je i na nivou baze)
 * @return void
 * @author MARKO
 */ 
    public function insertRate($idMeal,$idCustomer,$ocena1,$ocena2,$ocena3,$ocena4,$ocena5){
        $data = array(
        'IDMeal' => $idMeal,
        'IDCustomer' => $idCustomer,
        'Rate1' => $ocena1,
        'Rate2' => $ocena2,
        'Rate3' => $ocena3,
        'Rate4' => $ocena4,
        'Rate5' => $ocena5
        );
        $this->db->insert('rate_meal', $data);
    }
}

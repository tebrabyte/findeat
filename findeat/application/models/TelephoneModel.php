<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
* TelephoneModel je model koji je zaduzen za dohvatanje telefona iz tabele Telephone
*
* @author Filip Tanic
*/
class TelephoneModel extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
 /**
 * Dohvatanje telefona iz tabele Telephone na osnovu prosledjene adrese
 *
 * @return telephones[]
 */
    public function getTelephones($address){
        return $this->db->from('Telephone')->where('IDLocation',$address->IDLocation)->get()->result();
    }
}

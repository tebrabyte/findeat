<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Admin model je model koji je zaduzen za komuniciranje i dohvatanje podataka iz tabele administrator
 *
 * @author filip.tanic
 */
class AdminModel extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
 /**
 * Updatovanje polja Name i Surname administrator tabele na osnovu id administatora
 *
 * @return void
 */
    public function updateAdmin($user, $data){
        $this->db->set('Name', $data["name"]);
        $this->db->set('Surname', $data["surname"]);
        $this->db->where('IDAdministrator',$user->IDUser)->update('administrator');
    }
    
}

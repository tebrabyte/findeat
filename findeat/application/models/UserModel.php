<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Ovaj model sluzi za komunikaciju programa sa tabelom User
 *
 * @author Filip Tanic
 */
class UserModel extends CI_Model{
    
    public function __construct() {
        parent::__construct();
    }
    
 /**
 * Dohvatanje korisnika na osnovu korisnickog imena(moze biti i email)
 *
 * @return user
 */
    public function getUser($username){
        $user = $this->db->from('user')->where('user_name',$username)->or_where('e_mail',$username)->get()->row();
        return $user;
    }
	
 /**
 * Provera ispravnosti unete lozinke
 *
 * @return bool
 */
    public function checkPassword($user, $password){
        return ($password == $user->Password);
    }
    
 /**
 * Provera da li je prosledjeni korisnik Admin
 *
 * @return bool
 */
    public function isAdmin($user){
         return ($this->db->from('administrator')->where('IDAdministrator',$user->IDUser)->get()->row() != NULL);
    } 
    
 /**
 * Provera da li je prosledjeni korisnik ugostitelj(host)
 *
 * @return bool
 */
    public function isHost($user){
         return ($this->db->from('host_restaurant')->where('IDHost_Restaurant',$user->IDUser)->get()->row() != NULL);
    }
    
 /**
 * Dohvatanje imena korisnika ili restorana na osnovu korisnika
 *
 * @return String
 */
    public function getName($user,$usertype){
        if($usertype == 'admin'){
            $name = $this->db->from('administrator')->where('IDAdministrator',$user->IDUser)->get()->row()->Name;
        }
        elseif($usertype == 'host'){
            $name = $this->db->from('host_restaurant')->where('IDHost_Restaurant',$user->IDUser)->get()->row()->Name;
        }
        elseif($usertype == 'customer'){
            $name = $this->db->from('customer')->where('IDCustomer',$user->IDUser)->get()->row()->Name;
        }
        return $name;
    }
	
 /**
 * Dohvatanje prezimena korisnika ili imena restorana na osnovu korisnika
 *
 * @return String
 */
    public function getSurname($user,$usertype){
        if($usertype == 'admin'){
            $surnamename = $this->db->from('administrator')->where('IDAdministrator',$user->IDUser)->get()->row()->Surname;
        }
        elseif($usertype == 'host'){
            $surnamename = $this->db->from('host_restaurant')->where('IDHost_Restaurant',$user->IDUser)->get()->row()->Name;
        }
        elseif($usertype == 'customer'){
            $surnamename = $this->db->from('customer')->where('IDCustomer',$user->IDUser)->get()->row()->Surname;
        }
        return $surnamename;
    }
    
 /**
 * Brisanje korisnika
 *
 * @return void
 */
    public function deleteUser($user){
        $this->db->where('IDUser',$user->IDUser)->delete('user');
    }
    
 /**
 * Slanje email-a sa podacima korisnika na zadatu adresu (zaboravlejo korisnicko ime ili lozinka)
 *
 * @return void
 */
    public function sendUsernamePassword($email){
         $user = $this->db->from('user')->where('e_mail',$email)->get()->row();
         if($user != NULL){
             $config = array();
             $config['protocol'] = 'smtp';
             $config['smtp_host'] = 'ssl://smtp.googlemail.com';
             $config['smtp_user'] = 'findeatsupp@gmail.com';
             $config['smtp_pass'] = 'Tebra123Byte';
             $config['smtp_port'] = 465;
             $config['charset'] = 'iso-8859-1';
             $config['mailtype'] = 'html';
             //extension=php_openssl.dll

             $this->email->initialize($config);
             $this->email->set_newline("\r\n");
        
             $this->email->from('findeatsupp@gmail.com', 'FindEatSupport');
             $this->email->to("$email");
             $this->email->subject("Username and password for FindEat account");
             $this->email->message("<h2>Username: ".$user->User_name."</h2><h2>Password: ".$user->Password."</h2><p>All the best!</p>");
             
             $this->email->send();
        }
    }
    
 /**
 * Promena podataka korisnika na osnovu prosledjenih novih informacija
 *
 * @return void
 */
    public function updateUser($user, $data){
        $this->db->set('User_name', $data["username"]);
        $this->db->set('E_mail', $data["email"]);
        if($data["updatepassword"]){
            $this->db->set('Password', $data["newpass"]);
        }
        else{
            $this->db->set('Password', $data["oldpass"]);
        }
        $this->db->where('IDUser',$user->IDUser)->update('user');
    }
    
 /**
 * Promena da li korisnik za zadatim korisnickim imenom ili lozinkom vec postoji u bazi
 *
 * @return bool
 */
    public function checkExisting($user, $data){
        if($data["username"] != $user->User_name){
            $existing = $this->db->from('user')->where('User_name', $data["username"])->get()->row();
            if($existing != NULL)
                return false;
        }
        if($data["email"] != $user->E_mail){
            $existing = $this->db->from('user')->where('E_mail', $data["email"])->get()->row();
            if($existing != NULL)
                return false;
        }
        
        return true;
    }
    
 /**
 * Ubacivanje novog korisnika na osnovu prosledjenih informacija u tabelu User
 *
 * @return void
 */
    public function insertUser($data){
        $this->db->set('User_name', $data['username']);
        $this->db->set('Password', $data['pass']);
        $this->db->set('E_mail', $data['email']);
        $this->db->insert('user');
    }
    
 /**
 * Dohvatanje svih korisnika iz tabele User osim onih koji su Admini
 *
 * @return user[]
 */
    public function getAllUsersExceptAdmin(){
        
        $users = $this->db->from('user')->where_not_in('IDUser',1)->get()->result();
        return $users;
    }
    
}

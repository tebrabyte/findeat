<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Deprecated
 * RestaurantModel sluzi za komunikaciju programa sa tabelom Host_Restaurant
 *
 * @author marko.vekaric
 */
class RestaurantModel extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
/**
 * Deprecated
 * Dohvatanje ugostitelja(host) koji je vlasnik jela sa zadatim IDMeal-om
 * @return host_restaurant
 * @author marko.vekaric
 */
    public function getRestaurant($idMeal){
       $this->db->select('host_restaurant.IDHost_Restaurant, host_restaurant.Name, host_restaurant.Description, host_restaurant.Image, host_restaurant.Menu, host_restaurant.Rate');
       $this->db->from('host_restaurant');
       $this->db->join('meal', 'host_restaurant.IDHost_Restaurant = meal.IDHost_Restaurant');
       $this->db->where('meal.IDMeal', $idMeal);
       $query = $this->db->get();
       return $query->row();
    }
}

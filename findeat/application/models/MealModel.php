<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * MealModel je model koji je zaduzen za komunikaciju sa tabelom Meal
 *
 * @author sava.kezic
 */
class MealModel extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
 /**
 * Dohvatanje jela(meal) koja su po nazivu slicna prosledjenom teksu, sortirana po oceni
 *
 * @return meal[]
 */
    public function getMealsRate($text){
        return $this->db->from('meal')->like('Name',$text)->order_by('Rate','DESC')->get()->result();
    }
    
    /**
 * Dohvatanje jela(meal) koja su po nazivu slicna prosledjenom teksu, sortirana po ceni
 *
 * @return meal[]
 */
    public function getMealsPrice($text){
        return $this->db->from('meal')->like('Name',$text)->order_by('Price','DESC')->get()->result();
    }
    
 /**
 * Dohvatanje jela(meal) po porsledjenom IDMeal-u
 *
 * @return meal
 */
    public function getMealOverId($idmeal){
        return $this->db->from('Meal')->where('IDMeal',$idmeal)->get()->row();
    }
	
 /**
 * Dohvatanje jela(meal) za porsledjenog ugostitelja(host)
 *
 * @return meal[]
 */
    public function getHostMeals($host){
        return $this->db->from('meal')->where('IDHost_Restaurant',$host->IDHost_Restaurant)->get()->result();
    }
    
 /**
 * Dohvatanje ugostitelja(host) na osnovu prosledjenoh jela(meal)
 *
 * @return host
 */
    public function getHost($meal){
        $meal = $this->db->from('meal')->where('idmeal',$meal->IDMeal)->get()->row();
        return $this->db->from('host_restaurant')->where('IDHost_Restaurant',$meal->IDHost_Restaurant)->get()->row();
    }
    
 /**
 * Ubacivanje novog jela(meal) na osnovu prosledjenih informacija u tabelu Meal
 *
 * @return void
 */
    public function insertMeal($host, $data){
        $this->db->set('Name', $data['name']);
        $this->db->set('Description', $data['description']);
        $this->db->set('Price', $data['price']);
        $this->db->set('IDHost_Restaurant', $host->IDHost_Restaurant);
        $this->db->insert('meal');
        
        if($data['mealimage'] != NULL){
            $this->db->flush_cache();
            $idMeal = $this->db->from('meal')->where('IDHost_Restaurant', $host->IDHost_Restaurant)->order_by('IDMeal', 'DESC')->limit(1)->get()->row()->IDMeal;
            $this->db->query("update meal set Image ='".$data['mealimage']."' where IDMeal='".$idMeal."'");
        }
    }
    
 /**
 * Brisanje jela(meal) prosledjenih informacija iz tabele Meal
 *
 * @return void
 */
    public function deleteMeal($idMeal,$idUser){
        if($this->db->from('Meal')->where('IDMeal',$idMeal)->get()->row()->IDHost_Restaurant == $idUser)
            $this->db->where('IDMeal',$idMeal)->delete('meal');
    }
}

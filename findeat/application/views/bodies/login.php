
  <div class="container">
    <div class="row">
      <div class="col-sm-3">

      </div>
      <div class="col-sm-6">
        <img class="img-fluid" src="../../img/logo.png">
      </div>
      <div class="col-sm-3">

      </div>
    </div>

    <div class="row" style="padding-bottom:100px">
      <div class="col-sm-3">

      </div>
      <div class="col-sm-6">
            <form name="loginform" action="<?php echo site_url('Home/checklogin') ?>" method="post">
               <font color='red' size="4"><?php if(isset($errormsg)) echo $errormsg; ?></font>
                <div class="form-group">
                    <label class="col-form-label col-form-label-lg" for="inputLarge">Username</label>
                    <input class="form-control form-control-lg" name="username" type="text" value="<?php if(isset($username)) echo $username ?>" placeholder="Enter username here..." id="inputLarge">
                     <a href="<?php echo site_url('Home/forgotUsernamePassword') ?>"> Forgot username? </a><br>
                    <a href="<?php echo site_url('Home/register') ?>"> Not registered yet? </a><br>
                  
		</div>
                <div class="form-group">
                    <label class="col-form-label col-form-label-lg" for="inputLarge">Password</label>
                    <input class="form-control form-control-lg" name="password" type="password" placeholder="Enter password here..." id="inputLarge">
                    <a href="<?php echo site_url('Home/forgotUsernamePassword') ?>"> Forgot password?</a><br>
		</div>
		<button type="submit" class="btn btn-primary btn-lg">Login</button>
            </form>
        </div>
      <div class="col-sm-3">
      </div>
    </div>
  </div>







  <footer class="page-footer font-small bg-light">

  <div class="footer-copyright text-center py-3"><hr><i><font size="1px" color="gray">© Copyright 2019:Filip Tanic,Marko Vekaric,Sava Kezic i Jovan Kecojevic. Odsek za softversko inzenjerstvo Elektrotehničkog fakulteta Univerziteta u Beogradu<br>FindEatSupprot: findeatsupp@gmail.com</font></i>
  </div>
  </footer>

</body>


</html>

<!DOCTYPE html>
<html>
<head>
  <title>FindEat</title>
  <link rel="stylesheet" href="../../../css/theme.css">
  <link rel="stylesheet" href="../../../css/StarRating.css">
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../../css/caro.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">

    <a class="navbar-brand" href="<?php echo site_url('Home/home') ?>"><img src="../../../img/logo.png" width="250px" height="130px" alt="LOGO"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <font size="4"><a class="nav-link" href="<?php echo site_url('Home/home') ?>">Home<span class="sr-only">(current)</span></a></font>
        </li>
        
      <!--  <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="" role="button" aria-haspopup="true" aria-expanded="false">Restorani</a>
          <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 48px, 0px);">
            <a class="dropdown-item" href="pages/opstineRestorani/novibeograd.html">Novi Beograd</a>
            <a class="dropdown-item" href="pages/opstineRestorani/zemun.html">Zemun</a>
            <a class="dropdown-item" href="pages/opstineRestorani/starigrad.html">Stari grad</a>
            <a class="dropdown-item" href="pages/opstineRestorani/vozdovac.html">Vozdovac</a>
          </div>
        </li> -->
        <!--<li class="nav-item">
          <a class="nav-link" href="#">Dostava</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="pages/Onama.html">O nama</a>
        </li> -->
      </ul>
      <font size="4">Welcome, <?php echo $name?></font><font size="4" >|</font><a href="
          <?php 
          if(isset($_SESSION['usertype'])){
            if($_SESSION['usertype'] == 'admin')
                      echo site_url('Admin/Profile');
            if($_SESSION['usertype'] == 'customer')
                      echo site_url('Customer/Profile');
            if($_SESSION['usertype'] == 'host')
                      echo site_url('Home_Restaurant/Profile');
          }
          ?>
        "><font size="4">Profile</font></a><font size="4" >|</font></font><a href="<?php echo site_url('Home/logout') ?>"><font size="4">Logout</font></a>
      <!--<img src="img/user transparen.jpg" height="26px" width="40px" href="index.html" style="margin-right:10px">
      <img src="img/language/english.png" height="26px" width="40px" href="#">-->
    </div>
  </nav>

  <div class="container">
    <div class="row" align="center" style="margin-top:50px">

      <div style="margin-bottom:30px;" class="col-sm-12 col-md-12">
		
         <?php if($meal->Image != NULL) echo '<img class="img-fluid" src="data:image/jpeg;base64,'.base64_encode($meal->Image) .'"height = "30%" width = "30%" />';?>
        <h1><?php echo $meal->Name;?></h1>
        <a href=" <?php echo site_url("Customer/oneRestaurant/$meal->IDMeal")?>" ><h3>Restaurant: <?php echo $host->Name;?></h3></a>
        <h4><font color="<?php switch (true) {
                                        case $meal->Rate <= 1:
                                            $color = "#e01818" ;
                                            break;

                                        case $meal->Rate <= 2:
                                            $color = "#b75050" ;
                                            break;

                                        case $meal->Rate <= 3:
                                            $color = "#474947" ;
                                            break;
                                        
                                        case $meal->Rate <= 4:
                                            $color = "#6fce7a" ;
                                            break;
                                        
                                        case $meal->Rate <= 5:
                                            $color = "#10d327" ;
                                            break;

                                        default:
                                            $color = "#474947";
                                            break;
                                    }
                                    echo $color ?>">Overall mark: <?php echo "$meal->Rate"; ?>/5.0</font></h4>
        
      </div>
        
   
      <div class="col-sm-12 col-md-12">
        <form align="center" action="<?php echo site_url('Customer/putRateMeal') ?>" method="post">
        <hr>
        <div>
          <h1>Taste</h1>
          <input type="hidden" id="s1if" name="s1if" value="0">
          <x-star-rating name="s1" id="s1" value="0">
          </x-star-rating>
        </div>
        <hr>
        <div>
          <h1>Price</h1>
          <input type="hidden" id="s2if" name="s2if" value="0">
          <x-star-rating name="s2" id="s2" value="0">
          </x-star-rating>
        </div>
        <hr>
        <div>
          <h1>Appropriate meal size</h1>
          <input type="hidden" id="s3if" name="s3if" value="0">
          <x-star-rating name="s3" id="s3" value="0">
          </x-star-rating>
        </div>
        <hr>
        <div>
          <h1>Decoration</h1>
          <input type="hidden" id="s4if" name="s4if" value="0">
          <x-star-rating name="s4" id="s4" value="0">
          </x-star-rating>
        </div>
        <hr>
        <div>
          <h1>Overall impression</h1>
          <input type="hidden" id="s5if" name="s5if" value="0">
          <x-star-rating name="s5" id="s5" value="0">
          </x-star-rating>
        </div>
        <hr>
        <button type="submit" class="btn btn-primary btn-lg" name="mealrate">Confirm</button>
        </form>
      </div>


    </div>


  </div>







  <footer class="page-footer font-small bg-light">

  <div class="footer-copyright text-center py-3"><hr><i><font size="1px" color="gray">© Copyright 2019:Filip Tanic,Marko Vekaric,Sava Kezic i Jovan Kecojevic. Odsek za softversko inzenjerstvo Elektrotehničkog fakulteta Univerziteta u Beogradu<br>FindEatSupprot: findeatsupp@gmail.com</font></i>
  </div>
  </footer>
  <script src="../../../js/StarRating.js"></script>
  
</body>


</html>

<!DOCTYPE html>
<html>
<head>
  <title>FindEat</title>
  <link rel="stylesheet" href="../../../css/theme.css">
  <link rel="stylesheet" href="../../../css/StarRating.css">
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../../css/caro.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">

    <a class="navbar-brand" href="<?php echo site_url('Home/home') ?>"><img src="../../../img/logo.png" width="250px" height="130px" alt="LOGO"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <font size="4"><a class="nav-link" href="<?php echo site_url('Home/home') ?>">Home<span class="sr-only">(current)</span></a></font>
        </li>
        
      </ul>
      <font size="4">Welcome, <?php echo $name?></font><font size="4" >|</font><a href="
          <?php 
          if(isset($_SESSION['usertype'])){
            if($_SESSION['usertype'] == 'admin')
                      echo site_url('Admin/Profile');
            if($_SESSION['usertype'] == 'customer')
                      echo site_url('Customer/Profile');
            if($_SESSION['usertype'] == 'host')
                      echo site_url('Home_Restaurant/Profile');
          }
          ?>
        "><font size="4">Profile</font></a><font size="4" >|</font></font><a href="<?php echo site_url('Home/logout') ?>"><font size="4">Logout</font></a>
     
    </div>
  </nav>

  <div class="container">
    <div class="row" align="center" style="margin-top:50px">

      <div style="margin-bottom:30px;" class="col-sm-12 col-md-12">
        
        <h1><?php echo "$host->Name"; ?></h1>
         <?php if($host->Image != NULL) echo '<img class="img-fluid" src="data:image/jpeg;base64,'.base64_encode($host->Image) .'"height = "40%" width = "40%" />';?>
        <br></br>
        <h3><font color="<?php switch (true) {
                                        case $host->Rate <= 1:
                                            $color = "#e01818" ;
                                            break;

                                        case $host->Rate <= 2:
                                            $color = "#b75050" ;
                                            break;

                                        case $host->Rate <= 3:
                                            $color = "#474947" ;
                                            break;
                                        
                                        case $host->Rate <= 4:
                                            $color = "#6fce7a" ;
                                            break;
                                        
                                        case $host->Rate <= 5:
                                            $color = "#10d327" ;
                                            break;

                                        default:
                                            $color = "#474947";
                                            break;
                                    }
                                   echo $color ?>">Overall mark: <?php echo "$host->Rate"; ?>/5.0</font></h3>
        <br>
         <p></p>
         <div class="card border-primary mb-3" style="max-width: 20rem;padding:'0px';margin:'0px';">
            <div class="card-body" style="padding:'0px';margin:'0px';">
              <p class="card-text" style="padding:'0px';margin:'0px';">
            
        <?php 
        
            
            foreach($addresses as $key => $address){
                echo "$address->Address</br>";
                foreach($telephones[$key] as $telephone){
                    echo "$telephone->Number</br>";
                }
                if(($key+1) < count($addresses)) echo '<br>';
            }
        ?>
             </p>
          </div>
        </div>
	<?php echo "<h5>$host->Description</5>"; ?>
          
        
        <br>
        <!--KOD ZA DOWNLOAD -->
       
        <!-- KOD ZA KRAJ DOWLOADA --> 
      </div>

      <div class="col-sm-12 col-md-12">
          
        
<?php

        if(isset($meals)){
            echo '<h1>Meals:</h1><hr>';
            foreach($meals as $key => $meal){
                echo "<div><h1>$meal->Name</h1>";
                if($meal->Image != NULL) 
                    echo '<p><img src="data:image/jpeg;base64,'.base64_encode($meal->Image) .'"height = "30%" width = "30%" /></p>';
                echo "<p>".$meal->Description."</p>";
                echo "<h5>Price: ".$meal->Price." RSD</h5>";
                echo "<h5>Overall mark: ".$meal->Rate."/5.0</h5>";
                echo "</div>";
                echo "<hr>";
            }
        }     
?>
<?php
        if(isset($comments)){
            echo '<h3>Comments from other users:</h3><br><br>';
            foreach($comments as $key => $comment){
                echo'<div class="card border-primary mb-3" style="max-width: 20rem;">';
                echo '<div class="card-header"><b>'.$comment->Name.' '.$comment->Surname.'</b></div>';
                echo '<div class="card-body">';
                echo '<p class="card-text">'.$comment->Text.'</p>';
                if(isset($_SESSION['usertype']) && $_SESSION['usertype']== 'admin')
                    echo '<a href="'.site_url("Admin/deleteComment/$comment->IDComment").'">Delete coment</a>';
                echo '</div>';
                echo '</div>';
                echo "<br>";
            }
        }     
    
        if($_SESSION['usertype'] == 'customer'){
            echo '<form align="center" action="'.site_url('Customer/putRateRestaurant').'" method="post">';
            echo '<div>';
            echo  '<h1>Rate our restaurant!</h1>';
            echo  '<input type="hidden" id="s1rp" name="s1rp" value="0">';
            echo  '<x-star-rating name="s1r" id="s1r" value="0">';
            echo  '</x-star-rating>';
            echo '</div>';

            echo '<div class="form-group">';
            echo '<textarea name="description" class="form-control" id="exampleTextarea" rows="7" placeholder="Comment..."></textarea>';
            echo '</div>';
            echo '<button type="submit" class="btn btn-primary btn-lg" name="mealrate">Rate</button>';

            echo '</form>';
        }
 ?>          
        
      </div>

      
    </div>


  </div>







  <footer class="page-footer font-small bg-light">

  <div class="footer-copyright text-center py-3"><hr><i><font size="1px" color="gray">© Copyright 2019:Filip Tanic,Marko Vekaric,Sava Kezic i Jovan Kecojevic. Odsek za softversko inzenjerstvo Elektrotehničkog fakulteta Univerziteta u Beogradu<br>FindEatSupprot: findeatsupp@gmail.com</font></i>
  </div>
  </footer>
 <script src="../../../js/StarRating2.js"></script>
</body>


</html>

<div class="container">
    <div class="row">
      <div class="col-sm-3">

      </div>
      <div class="col-sm-6">
        <img class="img-fluid" src="../../img/logo.png">
        <h1><?php echo $host->Name;?></h1>
      </div>
      <div class="col-sm-3">

      </div>
    </div>

    <div class="row" style="padding-bottom:100px">
      <div class="col-sm-3">
      </div>
      <div class="col-sm-6">
	  <form name="formaPromena" action ="
                <?php 
                if(isset($_SESSION['usertype'])){
                    if($_SESSION['usertype'] == 'admin')
                        echo site_url('Admin/editProfile');
                   if($_SESSION['usertype'] == 'customer')
                        echo site_url('Customer/editProfile');
                   if($_SESSION['usertype'] == 'host')
                        echo site_url('Host_Restaurant/editRestaurantInfo');
                } ?>
                " method="post" enctype="multipart/form-data">
        <div class="form-group">
        <label for="exampleInputFile">Change Image</label>
        <input name="newimage" type="file" class="form-control-file" id="newimage" aria-describedby="fileHelp">
        <small id="fileHelp" class="form-text text-muted">Restaurant Image</small>
        </div>
	<div class="form-group">
        <label for="exampleInputFile">Change Menu</label>
        <input name="newmenu" type="file" class="form-control-file" id="newmenu" aria-describedby="fileHelp">
        <small id="fileHelp" class="form-text text-muted">Restaurant Menu in PDF format</small>
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Username</label>
        <input class="form-control form-control-lg" name="username" type="text" value="<?php echo $user->User_name;?>" placeholder="Enter username here..." id="inputLarge">
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">E-mail</label>
        <input class="form-control form-control-lg" name="email" type="text" value="<?php echo $user->E_mail;?>" placeholder="Enter e-mail here..." id="inputLarge">
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Old Password</label>
        <input class="form-control form-control-lg" name="oldpass" type="password" placeholder="Enter password here..." id="inputLarge" required>
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">New Password</label>
        <input class="form-control form-control-lg" name="newpass" type="password" placeholder="Enter new password here..." id="inputLarge">
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Re-password</label>
        <input class="form-control form-control-lg" name="repass" type="password" placeholder="Repeat new password here..." id="inputLarge">
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Restaurant Name</label>
        <input class="form-control form-control-lg" name="name" type="text" value="<?php echo $host->Name;?>" placeholder="Enter name here..." id="inputLarge">
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">New Address</label>
        <input class="form-control form-control-lg" name="newaddress" type="text" placeholder="Enter new addres here..." id="inputLarge">
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">New Telephone</label>
        <input class="form-control form-control-lg" name="newtelephone" type="number" placeholder="Enter new telephone here..." id="inputLarge">
        </div>
        <div class="form-group">
        <label for="exampleTextarea">Restaurant description</label>
	<textarea name="description" class="form-control" id="exampleTextarea" rows="7"><?php echo $host->Description;?></textarea>
	</div>

         <hr>
         <button  type="submit" style="margin-right:30px" class="btn btn-primary btn-lg">Submit</button>
	</form>
        <form name="deleteprofile" action="<?php echo site_url('Home/deleteProfile') ?>" method="post">
         <?php if ($_SESSION['usertype'] != 'admin'){
            echo'<hr>';
            echo'<h3>Do you want to delete your profile?</h3>';
            echo'<button  type="submit" class="btn btn-primary btn-lg">Delete Profile</button>';
            echo'<hr>';
         }
         ?>
	</form>
        </div>

      </div>
      <div class="col-sm-3">

      </div>
    </div>
 </div>






  <footer class="page-footer font-small bg-light">

  <div class="footer-copyright text-center py-3"><hr><i><font size="1px" color="gray">© Copyright 2019:Filip Tanic,Marko Vekaric,Sava Kezic i Jovan Kecojevic. Odsek za softversko inzenjerstvo Elektrotehničkog fakulteta Univerziteta u Beogradu</font></i>
  </div>
  </footer>

</body>

</html>

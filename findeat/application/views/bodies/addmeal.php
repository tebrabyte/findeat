

  <div class="container">
    <div  style="margin-top:30px;" class="row">
      <div class="col-sm-3">

      </div>
      <div class="col-sm-6">
        <h1>Add meal</h1>
        <h3><?php echo $host->Name;?></h3>
      </div>
      <div class="col-sm-3">

      </div>
    </div>

    <div class="row" style="margin-top:30px;" style="padding-bottom:100px">
      <div class="col-sm-3">

      </div>

      <div class="col-sm-6">
        <form action="addMeal" name="formAddMeal" method="post" enctype="multipart/form-data">
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Name of the meal</label>
        <input class="form-control form-control-lg" name="name" type="text" placeholder="Enter name of the meal here..." id="inputLarge" required>
        </div>
        <div class="form-group">
        <label for="exampleInputFile">Add Picture</label>
        <input name="mealimage" type="file" id="mealimage" class="form-control-file" aria-describedby="fileHelp">
        </div>
        <div class="form-group">
        <label for="exampleTextarea">Description</label>
        <textarea name="description" class="form-control" id="exampleTextarea" rows="3"></textarea>
        </div>
        <div class="form-group">
        <label class="col-form-label col-form-label-lg" for="inputLarge">Price of the meal</label>
        <input class="form-control form-control-lg" name="price" type="number" placeholder="Enter price of the meal here..." id="inputLarge" required>
        </div>
        <button type="submit" class="btn btn-primary btn-lg">Add Meal</button>
        </form>
      </div>
      <div class="col-sm-3">

      </div>
    </div>
  </div>







  <footer class="page-footer font-small bg-light">

  <div class="footer-copyright text-center py-3"><hr><i><font size="1px" color="gray">© Copyright 2019:Filip Tanic,Marko Vekaric,Sava Kezic i Jovan Kecojevic. Odsek za softversko inzenjerstvo Elektrotehničkog fakulteta Univerziteta u Beogradu</font></i>
  </div>
  </footer>

</body>


</html>

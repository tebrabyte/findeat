
  
  <div class="view" style="background-image: url('../../img/hdpozadina.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
         <!-- Mask & flexbox options-->
          <div class="mask rgba-black-light align-items-center">
            <!-- Content -->
            <div class="container">
              <!--Grid row-->
              <div class="row" style="padding-top:100px">
                <!--Grid column-->
                <div class="col-md-12 mb-4 white-text text-center">
                  <h1 class="h1-reponsive white-text text-uppercase font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInDown" data-wow-delay="0.3s"><strong><font color="white">SEARCH YOUR MEAL</font></strong></h1>
                  <!––<hr class="hr-light my-4 wow fadeInDown" data-wow-delay="0.4s">
                </div>
                <!--Grid column-->
              </div>
              <div class="row" style="padding-bottom:300px">
                <div class="col-sm-4">

                </div>
                <div class="col-sm-4">
                  <form align="center" action="<?php echo site_url('Home/search') ?>" method="post">
                  <input name="searchName" class="form-control form-control-lg" type="text" placeholder="Desired meal" id="inputLarge">
                  <br>
                  <div class="form-group">
                    <select class="form-control" name="dropdown" id="exampleSelect1">
                      <option value="sortby">Sort by</option>
                      <option value="rating">Rating</option>
                      <option value="price">Price</option>
                      <option value="location">Location</option>
                    </select>
                  </div>
                  <button style="margin-top:15px;"name="searchDugme" type="submit" class="btn btn-primary btn-lg">Search</button>
                  </form>
                </div>
                <div class="col-sm-4">
				
                </div>
              </div>
              <!--Grid row-->
            </div>
            <!-- Content -->
          </div>
        </div>
		
			<?php
				if(isset($_SESSION['meals'])){
                                    echo "<br>";
                                    echo "<br>";
                                    echo "<br>";
                                    echo '<div class="container">';
                                    foreach($_SESSION['meals'] as $key => $meal){
                                        echo '<div align="center" class="row">';
                                        echo '<div class="col-sm-3 col-md-3"></div>';
                                        echo '<div align="center" class="col-sm-6 col-md-6">';
                                    if($meal->Image != NULL){ 
                                    echo '<img src="data:image/jpeg;base64,'.base64_encode($meal->Image) .'"height = "350px" width = "100%" />';
                                    echo '<br>';
                                    echo '<br>';
                                   
                                    }
                                    switch (true) {
                                        case $meal->Rate <= 1:
                                            $color = "#e01818" ;
                                            break;

                                        case $meal->Rate <= 2:
                                            $color = "#b75050" ;
                                            break;

                                        case $meal->Rate <= 3:
                                            $color = "#474947" ;
                                            break;
                                        
                                        case $meal->Rate <= 4:
                                            $color = "#6fce7a" ;
                                            break;
                                        
                                        case $meal->Rate <= 5:
                                            $color = "#10d327" ;
                                            break;

                                        default:
                                            $color = "#474947";
                                            break;
                                    }
                                    echo "<h1>".$meal->Name."</h1><h2>Price: ".$meal->Price." RSD </h2><p>".$meal->Description."</p><h3><font color=".$color.">Rate: ".$meal->Rate."/5.0</font></h3>";
                                    $idhost = $_SESSION['restaurants'][$key]->IDHost_Restaurant;
                                    if(isset($_SESSION['usertype'])){
                                        if($_SESSION['usertype'] == 'customer')
                                            echo "<h4>at <a href=".site_url("Customer/oneRestaurant/$meal->IDMeal").">".$_SESSION['restaurants'][$key]->Name."</a></h4>";
                                        if($_SESSION['usertype'] == 'admin')
                                            echo "<h4>at <a href=".site_url("Admin/oneRestaurant/$meal->IDMeal").">".$_SESSION['restaurants'][$key]->Name."</a></h4>";
                                    }
                                    else{
                                        echo "<h4>at <a href=".site_url("Home/oneRestaurant/$idhost").">".$_SESSION['restaurants'][$key]->Name."</a></h4>";
                                    }
                                    if(isset($_SESSION['user']) && ($_SESSION['usertype'] == 'customer')){
					echo "<a href=".site_url("Customer/ratemeal/$key").">Rate meal</a>";
                                    }
                                    echo'</div>';
                                    echo '</div>';
                                    echo '<div class="col-sm-3 col-md-3"></div>';
                                    echo '<hr>'; 
                                    }
                                    echo '</div>';
                                }
			?>
		  
		
		
  <footer class="page-footer font-small bg-light">
  <div class="footer-copyright text-center py-3"><i><font size="1px" color="gray">© Copyright 2019:Filip Tanic,Marko Vekaric,Sava Kezic i Jovan Kecojevic. Odsek za softversko inzenjerstvo Elektrotehničkog fakulteta Univerziteta u Beogradu<br>FindEatSupprot: findeatsupp@gmail.com</font></i>
  </div>
  </footer>	
</body>


</html>

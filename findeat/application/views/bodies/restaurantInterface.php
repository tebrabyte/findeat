

  <div class="container">
    <div class="row" align="center" style="margin-top:50px">

      <div style="margin-bottom:30px;" class="col-sm-12 col-md-12">
        <!--<h1>Welcome ime korisnika</h1>-->

        <h1><?php echo "$host->Name"; ?></h1>
        <?php if($host->Image != NULL) echo '<img class="img-fluid" src="data:image/jpeg;base64,'.base64_encode($host->Image) .'"height = "400" width = "400" />';?>
        <br></br>
        <h3><font color="<?php switch (true) {
                                        case $host->Rate <= 1:
                                            $color = "#e01818" ;
                                            break;

                                        case $host->Rate <= 2:
                                            $color = "#b75050" ;
                                            break;

                                        case $host->Rate <= 3:
                                            $color = "#474947" ;
                                            break;
                                        
                                        case $host->Rate <= 4:
                                            $color = "#6fce7a" ;
                                            break;
                                        
                                        case $host->Rate <= 5:
                                            $color = "#10d327" ;
                                            break;

                                        default:
                                            $color = "#474947";
                                            break;
                                    }
                                   echo $color ?>">Overall mark: <?php echo "$host->Rate"; ?>/5.0</font></h3>
        <p></p>
        <div class="card border-primary mb-3" style="max-width: 20rem;padding:'0px';margin:'0px';">
            <div class="card-body" style="padding:'0px';margin:'0px';">
              <p class="card-text" style="padding:'0px';margin:'0px';">
        <?php 
            foreach($addresses as $key => $address){
                echo "$address->Address</br>";
                foreach($telephones[$key] as $telephone){
                    echo "$telephone->Number</br>";
                }
                if(($key+1) < count($addresses)) echo '<br>';
            }
        ?>
              </p>
          </div>
        </div>
	<?php echo "<h5>$host->Description</5>"; ?>
      </div>
      <div class="col-sm-12 col-md-12">

        <h1>Meals:</h1>
        <hr>
<?php
        if(isset($meals)){
            foreach($meals as $key => $meal){
                echo "<div><h1>$meal->Name</h1>";
                if($meal->Image != NULL) 
                    echo '<p><img src="data:image/jpeg;base64,'.base64_encode($meal->Image) .'"height = "250" width = "250" /></p>';
                echo "<p>".$meal->Description."</p>";
                echo "<h5>Price: ".$meal->Price." RSD</h5>";
                echo "<h5>Overall mark: ".$meal->Rate."/5.0</h5>";
                echo "</div>";
				echo '<a href="'.site_url("Host_Restaurant/deleteMeal/$meal->IDMeal").'">Delete meal</a>';
                echo "<hr>";
            }
        }
            
?>
       
      <button type="button" onclick="location.href='<?php echo site_url("Host_Restaurant/showAddMeal"); ?>';"  class="btn btn-primary btn-lg" name="addMeal"
        <?php
        if(isset($meals) && count($meals)>4)
            echo "disabled";
        ?>
      >Add meal</button>
         <p></p>
        <?php
        if(isset($meals) && count($meals)>4)
            echo "<p><font color='red'> You have reached the maximum number of meals</font></p>";
        ?>
         <hr><br>
 <?php
        if(isset($comments)){
            echo '<h3>Comments from other users:</h3><br><br>';
            foreach($comments as $key => $comment){
                echo'<div class="card border-primary mb-3" style="max-width: 20rem;">';
                echo '<div class="card-header"><b>'.$comment->Name.' '.$comment->Surname.'</b></div>';
                echo '<div class="card-body">';
                echo '<p class="card-text">'.$comment->Text.'</p>';
                echo '</div>';
                echo '</div>';
                echo "<br>";
            }
        }     
?> 
      </div>
    </div>
  </div>







  <footer class="page-footer font-small bg-light">

  <div class="footer-copyright text-center py-3"><hr><i><font size="1px" color="gray">© Copyright 2019:Filip Tanic,Marko Vekaric,Sava Kezic i Jovan Kecojevic. Odsek za softversko inzenjerstvo Elektrotehničkog fakulteta Univerziteta u Beogradu<br>FindEatSupprot: findeatsupp@gmail.com</font></i>
  </div>
  </footer>
 
</body>


</html>

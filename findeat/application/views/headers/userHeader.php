<!DOCTYPE html>
<html>
<head>
  <title>FindEat</title>
  <link rel="stylesheet" href="../../css/theme.css">
  <link rel="stylesheet" href="../../css/StarRating.css">
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../css/caro.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">

    <a class="navbar-brand" href="<?php echo site_url('Home/home') ?>"><img src="../../img/logo.png" width="250px" height="130px" alt="LOGO"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <font size="4"><a class="nav-link" href="<?php echo site_url('Home/home') ?>">Home<span class="sr-only">(current)</span></a></font>
        </li>
        
      <!--  <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="" role="button" aria-haspopup="true" aria-expanded="false">Restorani</a>
          <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 48px, 0px);">
            <a class="dropdown-item" href="pages/opstineRestorani/novibeograd.html">Novi Beograd</a>
            <a class="dropdown-item" href="pages/opstineRestorani/zemun.html">Zemun</a>
            <a class="dropdown-item" href="pages/opstineRestorani/starigrad.html">Stari grad</a>
            <a class="dropdown-item" href="pages/opstineRestorani/vozdovac.html">Vozdovac</a>
          </div>
        </li> -->
        <!--<li class="nav-item">
          <a class="nav-link" href="#">Dostava</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="pages/Onama.html">O nama</a>
        </li> -->
      </ul>
      <font size="4">Welcome, <?php echo $name?></font><font size="4" >|</font><a href="
          <?php 
          if(isset($_SESSION['usertype'])){
            if($_SESSION['usertype'] == 'admin')
                      echo site_url('Admin/Profile');
            if($_SESSION['usertype'] == 'customer')
                      echo site_url('Customer/Profile');
            if($_SESSION['usertype'] == 'host')
                      echo site_url('Host_Restaurant/editProfile');
          }
          ?>
        "><font size="4">Profile</font></a><font size="4" >|</font></font><a href="<?php echo site_url('Home/logout') ?>"><font size="4">Logout</font></a>
      <!--<img src="img/user transparen.jpg" height="26px" width="40px" href="index.html" style="margin-right:10px">
      <img src="img/language/english.png" height="26px" width="40px" href="#">-->
    </div>
  </nav>
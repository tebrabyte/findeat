<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Customer
 *
 * @author Sava Kezic 0158/2016
 * 
 * Klasa kotrolera zaduzena za funkcionalnosti ulogovanog korisnika
 */
class Customer extends CI_Controller{
	/**
	* Kreiranje nove instance
	*
	* @return void
	*/
     public function __construct() {
        parent::__construct();
        
        $this->load->model('UserModel');
        $this->load->model('CustomerModel');
        $this->load->model('MealModel');
        $this->load->model('RateMealModel');
        $this->load->model('RestaurantModel');
        $this->load->model('HostModel');
        $this->load->model('RateRestaurantModel');
        $this->load->model('TelephoneModel');
        $this->load->model('CommentModel');
        
        if(isset($_SESSION['user'])){
            if($_SESSION['usertype'] == 'admin')
                redirect('Admin/index');
            if($_SESSION['usertype'] == 'host')
                redirect('Host_Restaurant/index');
        }
        else{
            redirect('Home/home');
        }
        
    }
	/**
	*Pocetna stranica customera.
	 
	* @return void
	*/
    public function index(){
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $this->load->view('headers/userHeader.php',['name'=>$name]);
        $this->load->view('bodies/home.php');
    }
    /**
	*Funkcija koja prikazuje stranicu za ocenjvanje odabranog jela.
	 
	* @return void
	*/
    public function ratemeal($key){
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $meal = $_SESSION['meals'][$key];
        $host = $this->MealModel->getHost($meal);
        $_SESSION['curMeal'] = $meal;
 
        $this->load->view('bodies/rateMeal.php',['name'=>$name,'meal'=>$meal,'host'=>$host]);
    }
    /**
	*Funkcija koja omogucava registrocanje ocene jela od strane customeru.
	 
	* @return void
	*/
    public function putRateMeal(){
        
        $ocena1=$this->input->post('s1if');
        if ($ocena1=='0') $ocena1='1';
        $ocena2=$this->input->post('s2if');
         if ($ocena2=='0') $ocena2='1';
        $ocena3=$this->input->post('s3if');
         if ($ocena3=='0') $ocena3='1';
        $ocena4=$this->input->post('s4if');
         if ($ocena4=='0') $ocena4='1';
        $ocena5=$this->input->post('s5if');
         if ($ocena5=='0') $ocena5='1';
        
         $idRatedMeal=$_SESSION['curMeal']->IDMeal;
         $idCustomer=$_SESSION['user']->IDUser;
         $this->RateMealModel->deleteRate($idRatedMeal,$idCustomer);
         $this->RateMealModel->insertRate($idRatedMeal,$idCustomer,$ocena1,$ocena2,$ocena3,$ocena4,$ocena5);
         
         unset($_SESSION['meals']);
         unset($_SESSION['meal']);
         
         
        
        Redirect('Home/home');
    }
    /**
	*Funkcija koja omogucava registrocanje ocene restorana od strane customeru.
	 
	* @return void
	*/
    public function putRateRestaurant(){
        $ocenaR=$this->input->post('s1rp');
        if ($ocenaR=='0') $ocenaR='1';
        $idRatedRestaurant=$_SESSION['curRestaurant']->IDHost_Restaurant;
        $idCustomer=$_SESSION['user']->IDUser;
        $comment = $this->input->post('description');
        
         $this->RateRestaurantModel->deleteRate($idRatedRestaurant,$idCustomer);
         $this->RateRestaurantModel->insertRate($idRatedRestaurant,$idCustomer,$ocenaR,$comment);
         
         Redirect('Home/home');
    }
    /**
	*Funkcija za prikazivanje informacija o customeru.
	 
	* @return void
	*/
    public function Profile(){
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $surname = $this->UserModel->getSurname($_SESSION['user'],$_SESSION['usertype']);
        $this->load->view('headers/userHeader.php',['name'=>$name]);
        $this->load->view('bodies/editProfile.php',['name'=>$name, 'surname'=>$surname]);
    }
    /**
	*Funkcija koja omogucava promenu informacija o customeru.
	 
	* @return void
	*/
    public function editProfile(){
        $receivedData["username"] = $this->input->post('username');
        $receivedData["email"] = $this->input->post('email');
        $receivedData["oldpass"] = $this->input->post('oldpass');
        $receivedData["oldpass"] = hash('sha256', base64_encode($receivedData["oldpass"]), false);
        $receivedData["newpass"] = $this->input->post('newpass');
        $receivedData["repass"] = $this->input->post('repass');
        $receivedData["name"] = $this->input->post('name');
        $receivedData["surname"] = $this->input->post('surname');
        $receivedData["updatepassword"] = $receivedData["newpass"] != NULL || $receivedData["repass"] != NULL;
        
        if($this->checkForChange($receivedData)){
            if($this->checkPassword($receivedData) && $this->UserModel->checkExisting($_SESSION['user'], $receivedData)){
                if($receivedData["newpass"] != NULL)
                    $receivedData["newpass"] = hash('sha256', base64_encode($receivedData["newpass"]), false);
                
                $this->UserModel->updateUser($_SESSION['user'], $receivedData);
                $this->CustomerModel->updateCustomer($_SESSION['user'], $receivedData);

                $_SESSION['user'] = $this->UserModel->getUser($receivedData["username"]);
                redirect('Customer/index');
            }
            else
                redirect('Customer/Profile');
        }
        else
            redirect ('Customer/index');
    }
    /**
	*Pomocna funkcija za promenu podataka o profilu customera, tj provera da li ima bilo kakvih promena koje treba da budu zapamcene u bazi.
	 
	* @return boolean
	*/
    private function checkForChange($receivedData){
    
        if($receivedData["username"] != $_SESSION['user']->User_name)
            return true;
        if($receivedData["email"] != $_SESSION['user']->E_mail)
            return true;
        if($receivedData["name"] != $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']))
            return true;
        if($receivedData["surname"] != $this->UserModel->getSurname($_SESSION['user'],$_SESSION['usertype']))
            return true;
        if($receivedData["newpass"] != NULL || $receivedData["repass"] != NULL)
            return true;
        
        return false;
    }
    /**
	*Pomocna funkcija za promenu podataka o profilu customera, tj. provera verifikacije customera.
	 
	* @return boolean
	*/
    private function checkPassword($receivedData){
        
        if($receivedData["oldpass"] != $_SESSION['user']->Password)
            return false;
        if($receivedData["newpass"] == NULL && $receivedData["repass"] == NULL)
            return true;
        if($receivedData["newpass"] != $receivedData["repass"])   
            return false;
        return true;
    }
    /**
	*Funcija koja prikazuje stranicu restorana koju customer zahteva.
	 
	* @return void
	*/
    public function oneRestaurant($idmeal){
        $meal=$this->MealModel->getMealOverId($idmeal);
        $host=$this->RestaurantModel->getRestaurant($meal->IDMeal);
        $_SESSION['curRestaurant']=$host;
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $hostmeals = $this->MealModel->getHostMeals($host);
        $addresses = $this->HostModel->getAddresses($host);
        $telephones = [];
        foreach($addresses as $key => $address){
            $telephones[$key] = $this->TelephoneModel->getTelephones($address);
        }
        $comments = $this->CommentModel->getCommentsForHost($host);
        $this->load->view('bodies/oneRestaurant.php',['name'=>$name,'host'=>$host,'meals'=>$hostmeals,'addresses'=>$addresses,'telephones'=>$telephones,'comments'=>$comments]);
    }
}

<?php

/**
 * Description of HomeController
 *
 * @author Sava Kezic 0158/2016
 
 * Home kotroler je zaduzen za registraciju korisnika, logovnje korisnika, brisanje naloga, proveru verifikacije korisnika, prikazivanja pocetne stranice, kao i prikazivanja rezultata pretrage.
 */
class Home extends CI_Controller{
    /**
	*Kreiranje nove instance
	*
	* @return void
	*/
    public function __construct() {
        parent::__construct();
        
        $this->load->model('UserModel');
        $this->load->model('MealModel');
        $this->load->model('CustomerModel');
        $this->load->model('HostModel');
        $this->load->model('TelephoneModel');
        $this->load->model('CommentModel');
       
    }
	/**
	*Redirekcija na pocetnu stranicu.
	 
	* @return void
	*/
    public function index(){
        redirect('Home/home');
    }
    /**
	*Prikaz pocetne stranice i  rezultata pretrage.
	 
	* @return void
	*/
    public function home(){
        $this->check();
       
        $this->load->view('headers/homeHeader.php');
        $this->load->view('bodies/home.php');
    }
    /**
	*Prikaz pocetne stranice za logovanje korisnika, kao i mogucnost dobijanja username-a i password-a na e-mail adresu.
	 
	* @return void
	*/
    public function login($message = NULL, $username = NULL){
        $this->check();
        
        $this->load->view('headers/homeHeader.php');
        $this->load->view('bodies/login.php',['errormsg'=>$message,'username'=>$username]);
    }
    /**
	*Provera validnosti podataka za logovanje korisnika.
	 
	* @return void
	*/
    public function checklogin(){
        $this->check();
        
        $this->form_validation->set_rules('username', "Username", "required");
        $this->form_validation->set_rules('password', "Password", "required");
        if($this->form_validation->run() == FALSE){
            $user = $this->input->post('username');
            $this->login("All fields are required!", $user);
        }  
        else{          
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $password = hash('sha256', base64_encode($password), false);
            //redirect('Home/greska/'.$password);
            $user = $this->UserModel->getUser($username);
            if($user == NULL){
                $this->login("No user found!");
            }
            else{
                if(!$this->UserModel->checkPassword($user, $password)){
                    $this->login("Wrong password!", $username);
                }
                else{
                    $_SESSION['user'] = $user;
                    if($this->UserModel->isAdmin($user)){
                       $_SESSION['usertype'] = 'admin';
                       unset($_SESSION['meals']);
                       redirect('Admin/index');
                    }
                    elseif($this->UserModel->isHost($user)){
                        $_SESSION['usertype'] = 'host';
                        unset($_SESSION['meals']);
                        redirect('Host_Restaurant/index');
                    }
                    else{
                        $_SESSION['usertype'] = 'customer';
                        unset($_SESSION['meals']);
                        redirect ('Customer/index');
                    }
                }
            }
        }
    }
    /**
	*Prikaz stranice za registrovanje korisnika.
	 
	* @return void
	*/
    public function register($message = NULL){
        $this->check();
        
        $this->load->view('headers/homeHeader.php');
        $this->load->view('bodies/register.php',['errormsg'=>$message]);
    }
    /**
	*Fukcija za logout-ovanje korisnika i brisanje podataka iz sesije o korisniku.
	 
	* @return void
	*/
     public function logout(){
        unset($_SESSION['user']);
        unset($_SESSION['usertype']);
        unset($_SESSION['meals']);
        redirect('Home/home');
    }
    /**
	*Bezbednosna funkcija koja ne dozvoljava korisniku pristup funkcijama koje mu nisu dozvoljene.
	 
	* @return void
	*/
    private function check(){
         if(isset($_SESSION['user'])){
            if($_SESSION['usertype'] == 'admin')
                redirect('Admin/index');
            if($_SESSION['usertype'] == 'customer')
                redirect('Customer/index');
            if($_SESSION['usertype'] == 'host')
                redirect('Host_Restaurant/index');
        }
    }
    /**
	*Funkcija koja u sesiju pamti rezultate pretrage i redirektuje na home stranicu koja ih ispisuje.
	 
	* @return void
	*/
    public function search(){
        $text = $this->input->post('searchName');
        if($text == NULL)
            $text = "";
        $meals = [];
        if($this->input->post('dropdown')=="rating"){
            $meals = $this->MealModel->getMealsRate($text);
        }
        else if($this->input->post('dropdown')=="location"){
            //$meals = $this->MealModel->getMealsLocation($text);
        }
        else if($this->input->post('dropdown')=="price"){
            $meals = $this->MealModel->getMealsPrice($text);
        }
        else{
            $meals = $this->MealModel->getMealsRate($text);
        }
        $_SESSION['meals'] = $meals;
        $restaurants = [];
        foreach ($meals as $key => $meal){
            $restaurants[$key] = $this->HostModel->getRestaurant($meal);
        }
        $_SESSION['restaurants'] = $restaurants;
        if(isset($_SESSION['user'])){
            if($_SESSION['usertype'] == 'admin')
                redirect('Admin/index');
            if($_SESSION['usertype'] == 'customer')
                redirect('Customer/index');
        }
        else{
            redirect('Home/home');
        }
    }
    /**
	Funkcija koja omogucava brisanje profila odredjenog korisnika i redirektuje se na home stranu.
	 
	* @return void
	*/
    public function deleteProfile(){
        $this->UserModel->deleteUser($_SESSION['user']);
        redirect('Home/logout');
    }
    /**
	*Funcija koja prikazuje stranicu neophodnu za unos e-mail poste za korisnika koji je zaboravio username ili password.
	 
	* @return void
	*/
    public function forgotUsernamePassword(){
        $this->check();
       
        $this->load->view('headers/homeHeader.php');
        $this->load->view('bodies/forgotUsernamePassword.php');
    }
    /**
	*Funcija koja omogucava slanje e-mail poruke u kojoj se nalaze username i password korisnika koji je to zagtevao.
	 
	* @return void
	*/
    public function recoverUsernamePassword(){
        //ne mora check() jer se poziva iz forgotsUsernamePassword
        $email = $this->input->post('email');
        $this->UserModel->sendUsernamePassword($email);
    
        redirect('Home/home');
    }
    /**
	*Funcija koja omogucava registrovanje korisnika, tj ubacivanje podataka o korisniku u bazu.
	 
	* @return void
	*/
    public function checkRegister(){
        $this->form_validation->set_rules('username', "Username", "required");
        $this->form_validation->set_rules('pass', "Password", "required");
        if($this->form_validation->run() == FALSE){
            $user = $this->input->post('username');
            redirect('Home/register');// OVO TREBA SREDITI DA VRATI NAZAD I DA PRIKAZE ERRMSG: 
        }
        
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $user1 = $this->UserModel->getUser($username);
        $user2 = $this->UserModel->getUser($email);
        if($user1 != null || $user2 != null)
            $this->register("Username or E-mail already in use!");
        else{
            $receivedData["username"] = $this->input->post('username');
            $receivedData["email"] = $this->input->post('email');
            $receivedData["pass"] = $this->input->post('pass');
            $receivedData["pass"] = hash('sha256', base64_encode($receivedData["pass"]), false);
            $receivedData["repass"] = $this->input->post('repass');
            $receivedData["repass"] = hash('sha256', base64_encode($receivedData["repass"]), false);

            if($receivedData["pass"] != $receivedData["repass"])
            {
                $this->register("Password do not match!"); 
            }
            else
            {
                $receivedData["type"] = $this->input->post('optionsRadios');
                if($receivedData["type"] == "cust"){
                    $receivedData["name"] = $this->input->post('name');
                    $receivedData["surname"] = $this->input->post('surname');
                }
                elseif ($receivedData["type"] == "rest") {
                    $receivedData["name"] = $this->input->post('name');
                    $receivedData["address"] = $this->input->post('address');
                    $receivedData["telephone"] = $this->input->post('telephone');
                    if($_FILES['picture'] != null)
                        $receivedData["picture"] = addslashes(file_get_contents($_FILES['picture']['tmp_name']));
                    if($_FILES['menu'] != null)
                        $receivedData["menu"] = addslashes(file_get_contents($_FILES['menu']['tmp_name']));
                }
                else{
                    redirect('Host_restaurant/greska'.'/'.'err');// NIKADA NECE UCI
                }
                $this->UserModel->insertUser($receivedData);
                if($receivedData["type"] == "cust"){
                    $this->CustomerModel->insertCustomer($receivedData);
                }
                else{
                    $this->HostModel->insertHost($receivedData);
                }
                redirect('Home/login');
            } 
        }
    }
    /**
	*Pomocna funkcija za ispisivanje greske.
	 
	* @return void
	*/
    private function greska($msg){
        $this->load->view("greska.php", ['msg'=>$msg]);
    }
    /**
	*Funcija koja prikazuje stranicu restorana koju korisnik zahteva.
	 
	* @return void
	*/
    public function oneRestaurant($idhost){
        $host=$this->HostModel->getHostOverId($idhost);
        $_SESSION['curRestaurant']=$host;
        $hostmeals = $this->MealModel->getHostMeals($host);
        $addresses = $this->HostModel->getAddresses($host);
        $telephones = [];
        foreach($addresses as $key => $address){
            $telephones[$key] = $this->TelephoneModel->getTelephones($address);
        }
        $comments = $this->CommentModel->getCommentsForHost($host);
        $this->load->view('bodies/oneRestaurantHome.php',['host'=>$host,'meals'=>$hostmeals,'addresses'=>$addresses,'telephones'=>$telephones,'comments'=>$comments]);
    }
}

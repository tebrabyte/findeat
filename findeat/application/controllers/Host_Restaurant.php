<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Host_Restaurant
 *
 * @author Sava Kezic 0158/2016
 * Klasa kotrolera zaduzena za funkcionalnosti ugostitelja
 */
class Host_Restaurant extends CI_Controller{
	/**
	* Kreiranje nove instance
	*
	* @return void
	*/
     public function __construct() {
        parent::__construct();
        
        $this->load->model('UserModel');
        $this->load->model('HostModel');
        $this->load->model('MealModel');
        $this->load->model('TelephoneModel');
        $this->load->model('CommentModel');
        
        
        if(isset($_SESSION['user'])){
            if($_SESSION['usertype'] == 'admin')
                redirect('Admin/index');
            if($_SESSION['usertype'] == 'customer')
                redirect('Customer/index');
        }
        else{
            redirect('Home/home');
        }
        
    }
	/**
	*Pocetna stranica hosta.
	 
	* @return void
	*/
    public function index(){
        redirect('Host_Restaurant/Profile');
    }
    /**
	*Funkcija za prikazivanje pocetne stranice hosta.
	 
	* @return void
	*/
     public function Profile(){
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $host = $this->HostModel->getHost($_SESSION['user']);
        $hostmeals = $this->MealModel->getHostMeals($host);
        $addresses = $this->HostModel->getAddresses($host);
        $telephones = [];
        foreach($addresses as $key => $address){
            $telephones[$key] = $this->TelephoneModel->getTelephones($address);
        }
        $comments = $this->CommentModel->getCommentsForHost($host);
        
        $this->load->view('headers/userHeader.php',['name'=>$host->Name]);
        $this->load->view('bodies/restaurantInterface.php',['host'=>$host,'meals'=>$hostmeals,'addresses'=>$addresses,'telephones'=>$telephones,'comments'=>$comments]); 
     }
    /**
	*Funkcija koja prikazuje stranicu sa informacijama o hostu i mogucnosti promene istih.
	 
	* @return void
	*/
    public function editProfile(){
        $host = $this->HostModel->getHost($_SESSION['user']);
        $hostmeals = $this->MealModel->getHostMeals($host);
         
        $this->load->view('headers/userHeader.php',['name'=>$host->Name]);
        $this->load->view('bodies/changeRestaurantInfo.php',['host'=>$host, 'user'=>$_SESSION['user']]);
    }
    /**
	*Funkcija koja omogucava promenu informacija o hostu.
	 
	* @return void
	*/
    public function editRestaurantInfo(){
	/*
	php.ini
	post_max_size = 100M
	upload_max_filesize = 100M
	enctype="multipart/form-data" //na nivou forme
	*/
	if($_FILES['newimage'] != null)
            $receivedData["newimage"] = addslashes(file_get_contents($_FILES['newimage']['tmp_name']));
	if($_FILES['newmenu'] != null)
            $receivedData["newmenu"] = addslashes(file_get_contents($_FILES['newmenu']['tmp_name']));
        $receivedData["username"] = $this->input->post('username');
        $receivedData["email"] = $this->input->post('email');
        $receivedData["oldpass"] = $this->input->post('oldpass');
        $receivedData["oldpass"] = hash('sha256', base64_encode($receivedData["oldpass"]), false);
        $receivedData["newpass"] = $this->input->post('newpass');
        $receivedData["repass"] = $this->input->post('repass');
        $receivedData["name"] = $this->input->post('name');
        $receivedData["newaddress"] = $this->input->post('newaddress');
        $receivedData["newtelephone"] = $this->input->post('newtelephone');
	$receivedData["description"] = $this->input->post('description');
                
        $receivedData["updatepassword"] = $receivedData["newpass"] != NULL || $receivedData["repass"] != NULL;		
        if($this->checkForChange($receivedData)){
            if($this->checkPassword($receivedData) && $this->UserModel->checkExisting($_SESSION['user'], $receivedData)){
                if($receivedData["newpass"] != NULL)
                    $receivedData["newpass"] = hash('sha256', base64_encode($receivedData["newpass"]), false);
                
                $this->UserModel->updateUser($_SESSION['user'], $receivedData);
                $this->HostModel->updateHost($_SESSION['user'], $receivedData);

                $_SESSION['user'] = $this->UserModel->getUser($receivedData["username"]);
                redirect('Host_restaurant/index');
            }
            else
                redirect('Host_restaurant/index');
        }
       else
            redirect ('Host_restaurant/index'); 
    }
   /**
	*Pomocna funkcija za promenu podataka o profilu hosta, tj provera da li ima bilo kakvih promena koje treba da budu zapamcene u bazi.
	 
	* @return boolean
	*/
   private function checkForChange($receivedData){
        $host = $this->HostModel->getHost($_SESSION['user']);
    
        if($receivedData["newimage"] != null)
            return true;
        if($receivedData["newmenu"] != null)
            return true;
        if($receivedData["username"] != $_SESSION['user']->User_name)
            return true;
        if($receivedData["email"] != $_SESSION['user']->E_mail)
            return true;
        if($receivedData["name"] != $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']))
            return true;
        $addresses = $this->HostModel->getAddresses($_SESSION['user']);
        $b = false;
        foreach ($addresses as $address) {
            if($address->AddressName == $receivedData["newaddress"]){
                $b = true;
                break;
            }
        }
        if(!$b)
            return true;
        if($receivedData["newpass"] != NULL || $receivedData["repass"] != NULL)
            return true;
        
        return false;
    }
    /**
	*Pomocna funkcija za promenu podataka o profilu hosta, tj. provera verifikacije hosta.
	 
	* @return boolean
	*/
    private function checkPassword($receivedData){
        
        if($receivedData["oldpass"] != $_SESSION['user']->Password)
            return false;
        if($receivedData["newpass"] == NULL && $receivedData["repass"] == NULL)
            return true;
        if($receivedData["newpass"] != $receivedData["repass"])   
            return false;
        
        return true;
    }
    /**
	*Pomocna funkcija za ispisivanje greske.
	 
	* @return void
	*/
    private function greska($msg){
        $this->load->view("greska.php", ['msg'=>$msg]);
    }
    /**
	*Funkcija koja prikazuje stranicu za dodavanje jela.
	 
	* @return void
	*/
    public function showAddMeal(){
        $host = $this->HostModel->getHost($_SESSION['user']);
        
        $this->load->view('headers/userHeader.php',['name'=>$host->Name]);
        $this->load->view('bodies/addmeal.php',['host'=>$host, 'user'=>$_SESSION['user']]);
    }
	/**
	*Funkcija koja omogucava dodavanje jela i pamcenje informacija o jelu u bazi.
	 
	* @return void
	*/
    public function addMeal(){
        /*
	php.ini
	post_max_size = 100M
	upload_max_filesize = 100M
	enctype="multipart/form-data" //na nivou forme
	*/
        if($this->input->post('name') != null){//da li hoce direktno da udje u metodu reseno
            $host = $this->HostModel->getHost($_SESSION['user']);
            if(count($this->MealModel->getHostMeals($host)) >= 5)
                redirect('Host_restaurant/index');

            $receivedData["name"] = $this->input->post('name');
            if($_FILES['mealimage'] != null)
                $receivedData["mealimage"] = addslashes(file_get_contents($_FILES['mealimage']['tmp_name']));
            $receivedData["description"] = $this->input->post('description');
            $receivedData["price"] = $this->input->post('price');

            $this->MealModel->insertMeal($host, $receivedData);
        }
        redirect('Host_Restaurant/index');
    }
    /**
	*Funkcija koja omogucava brisanje jela od strane hosta.
	 
	* @return void
	*/
    public function deleteMeal($idMeal){
	$this->MealModel->deleteMeal($idMeal,$_SESSION['user']->IDUser);
        
	redirect('Host_Restaurant/Profile');
    }
    
}

<?php

/**
 * Description of HomeController
 *
 * @author Sava Kezic 0158/2016
 
 * Admin kotroler zaduzen je za odgovornosti admina. Odnosno izlistavanje i brisanje korisnika, komentara takodje.
 */
class Admin extends CI_Controller{
	/**
	* Kreiranje nove instance
	*
	* @return void
	*/
    public function __construct() {
        parent::__construct();
        
        $this->load->model('UserModel');
        $this->load->model('AdminModel');
        $this->load->model('MealModel');
        $this->load->model('CommentModel');
        $this->load->model('TelephoneModel');
        $this->load->model('RestaurantModel');
        $this->load->model('HostModel');
        
        if(isset($_SESSION['user'])){
            if($_SESSION['usertype'] == 'customer')
                redirect('Customer/index');
            if($_SESSION['usertype'] == 'host')
                redirect('Host_Restaurant/index');
        }
        else{
            redirect('Home/home');
        }
        
    }
    /**
	*Funcija koja omogucava brisanje korisnika od strane admina.
	 
	* @return void
	*/
    public function deleteMe($key){
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $userToDelete = $_SESSION['allUsers'][$key];
        $this->UserModel->deleteUser($userToDelete);
        redirect('Admin/deleteUser');
    }
    /**
	*Funcija koja prikazuje stranicu sa korisnicima koje zelimo da obrisemo.
	 
	* @return void
	*/
    public function deleteUser(){
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $this->load->view('headers/adminHeader.php',['name'=>$name]);
        $_SESSION['allUsers']=$this->UserModel->getAllUsersExceptAdmin();
        $this->load->view('bodies/deleteUser.php');
        
    }
	/**
	*Pocetna stranica admina.
	 
	* @return void
	*/
    public function index(){
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $this->load->view('headers/adminHeader.php',['name'=>$name]);
        $this->load->view('bodies/home.php');
    }
	/**
	*Prikaz stranice za izmenu podataka o profilu ulogovanog admina.
	 
	* @return void
	*/
    public function Profile(){
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $surname = $this->UserModel->getSurname($_SESSION['user'],$_SESSION['usertype']);
        $this->load->view('headers/adminHeader.php',['name'=>$name]);
        $this->load->view('bodies/editProfile.php',['name'=>$name, 'surname'=>$surname]);
    }
    /**
	*Funkcija koja omogucava promenu podataka o adminu i belezenja promenjenih podataka u bazu, kao i proveru verifikacije.
	 
	* @return void
	*/
    public function editProfile(){
        $receivedData["username"] = $this->input->post('username');
        $receivedData["email"] = $this->input->post('email');
        $receivedData["oldpass"] = $this->input->post('oldpass');
        $receivedData["oldpass"] = hash('sha256', base64_encode($receivedData["oldpass"]), false);
        $receivedData["newpass"] = $this->input->post('newpass');
        $receivedData["repass"] = $this->input->post('repass');
        $receivedData["name"] = $this->input->post('name');
        $receivedData["surname"] = $this->input->post('surname');
        $receivedData["updatepassword"] = $receivedData["newpass"] != NULL || $receivedData["repass"] != NULL;
        
        if($this->checkForChange($receivedData)){
            if($this->checkPassword($receivedData) && $this->UserModel->checkExisting($_SESSION['user'], $receivedData)){
                if($receivedData["newpass"] != NULL)
                    $receivedData["newpass"] = hash('sha256', base64_encode($receivedData["newpass"]), false);
                
                $this->UserModel->updateUser($_SESSION['user'], $receivedData);
                $this->AdminModel->updateAdmin($_SESSION['user'], $receivedData);

                $_SESSION['user'] = $this->UserModel->getUser($receivedData["username"]);
                redirect('Admin/index');
            }
            else
                redirect('Admin/Profile');
        }
        else
            redirect ('Admin/index');
    }
    /**
	*Pomocna funkcija za promenu podataka o profilu admina, tj provera da li ima bilo kakvih promena koje treba da budu zapamcene u bazi.
	 
	* @return boolean
	*/
    private function checkForChange($receivedData){
    
        if($receivedData["username"] != $_SESSION['user']->User_name)
            return true;
        if($receivedData["email"] != $_SESSION['user']->E_mail)
            return true;
        if($receivedData["name"] != $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']))
            return true;
        if($receivedData["surname"] != $this->UserModel->getSurname($_SESSION['user'],$_SESSION['usertype']))
            return true;
        if($receivedData["newpass"] != NULL || $receivedData["repass"] != NULL)
            return true;
        
        return false;
    }
    /**
	*Pomocna funkcija za promenu podataka o profilu admina, tj. provera verifikacije admina.
	 
	* @return boolean
	*/
    private function checkPassword($receivedData){
        
        if($receivedData["oldpass"] != $_SESSION['user']->Password)
            return false;
        if($receivedData["newpass"] == NULL && $receivedData["repass"] == NULL)
            return true;
        if($receivedData["newpass"] != $receivedData["repass"])    
            return false;

        return true;
    }
    /**
	*Funcija koja prikazuje stranicu restorana koju admin zahteva.
	 
	* @return void
	*/
    public function oneRestaurant($idmeal){
        $meal=$this->MealModel->getMealOverId($idmeal);
        $host=$this->RestaurantModel->getRestaurant($meal->IDMeal);
        $_SESSION['curRestaurant']=$host;
        $name = $this->UserModel->getName($_SESSION['user'],$_SESSION['usertype']);
        $hostmeals = $this->MealModel->getHostMeals($host);
        $addresses = $this->HostModel->getAddresses($host);
        $telephones = [];
        foreach($addresses as $key => $address){
            $telephones[$key] = $this->TelephoneModel->getTelephones($address);
        }
        $comments = $this->CommentModel->getCommentsForHost($host);
        $this->load->view('bodies/oneRestaurant.php',['name'=>$name,'host'=>$host,'meals'=>$hostmeals,'addresses'=>$addresses,'telephones'=>$telephones,'comments'=>$comments]);
    }
    /**
	*Funcija koja omogucava brisanje komentara od strane admina.
	 
	* @return void
	*/
    public function deleteComment($idcomment){
        $this->db->where('IDComment',$idcomment)->delete('Comment');
        
        redirect('Admin/index');
    }
   
}

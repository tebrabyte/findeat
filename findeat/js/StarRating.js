class StarRating extends HTMLElement{
  get value(){
    return this.getAttribute('value');
  }
  set value(val){
      this.setAttribute('value',val);
      this.highlight(this.value-1);
      var $noviString=this.id+"if";
      document.getElementById($noviString).setAttribute('value',val);
  }

  highlight(index){
    this.stars.forEach((star,i)=>{
      star.classList.toggle('full',i<=index);
    });
  }
  constructor(){
    super();
    this.stars= [];
    for (let i=0;i<5;i++){
      let s=document.createElement('div');
      s.className='star';
      this.appendChild(s);
      this.stars.push(s);
    }
    this.value=0;
    this.addEventListener('mousemove', e => {
          let box= this.getBoundingClientRect(),
          starIndex=Math.floor((e.pageX-box.left)/box.width * this.stars.length);
          this.highlight(starIndex);
    });

    this.addEventListener('mouseout',() =>{
        this.value=this.value;

    });

    this.addEventListener('click', e => {
          let box= this.getBoundingClientRect(),
          starIndex=Math.floor((e.pageX-box.left)/box.width * this.stars.length);
          this.value=starIndex+1;
    });

  }
}

window.customElements.define('x-star-rating',StarRating);

CREATE SCHEMA FindEat;

USE FindEat;

CREATE TABLE Administrator
(
	IDAdministrator      INTEGER NOT NULL PRIMARY KEY,
	Name                 VARCHAR(20) NOT NULL,
	Surname              VARCHAR(20) NOT NULL
);

CREATE TABLE Comment
(
	Text                 LONGTEXT NOT NULL ,
	IDComment            INTEGER NOT NULL PRIMARY KEY
);

CREATE TABLE Customer
(
	IDCustomer           INTEGER NOT NULL PRIMARY KEY,
	Name                 VARCHAR(20) NOT NULL,
	Surname              VARCHAR(20) NOT NULL
);

CREATE TABLE Host_Restaurant
(
	IDHost_Restaurant    INTEGER NOT NULL PRIMARY KEY,
	Name                 VARCHAR(50) NOT NULL,
	Description          TEXT NULL,
	Image                LONGBLOB NULL,
	Menu                 LONGBLOB NULL,
	Rate                 DECIMAL(2,1) NULL DEFAULT 0
);

CREATE TABLE Location
(
	IDLocation           INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Address				 VARCHAR(100) NOT NULL,
	IDHost_Restaurant    INTEGER NOT NULL
)
 AUTO_INCREMENT = 1;

CREATE TABLE Meal
(
	IDMeal               INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Name                 VARCHAR(50) NOT NULL,
	Description          TEXT NULL,
	Image                LONGBLOB NULL,
	Price                DECIMAL(7,2) NOT NULL CHECK ( Price >= 1 ),
	Rate                 DECIMAL(2,1) NULL DEFAULT 0,
	IDHost_Restaurant    INTEGER NOT NULL
)
 AUTO_INCREMENT = 1;

CREATE TABLE Rate_Meal
(
	IDRate_Meal          INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	IDMeal               INTEGER NOT NULL,
	IDCustomer           INTEGER NULL,
	Rate1                INTEGER NOT NULL CHECK ( Rate1 BETWEEN 1 AND 5 ),
	Rate2                INTEGER NOT NULL CHECK ( Rate2 BETWEEN 1 AND 5 ),
	Rate3                INTEGER NOT NULL CHECK ( Rate3 BETWEEN 1 AND 5 ),
	Rate4                INTEGER NOT NULL CHECK ( Rate4 BETWEEN 1 AND 5 ),
	Rate5                INTEGER NOT NULL CHECK ( Rate5 BETWEEN 1 AND 5 )
)
 AUTO_INCREMENT = 1;

CREATE UNIQUE INDEX Unique_IDGuest_IDMeal ON Rate_Meal
(
	IDCustomer ASC,
	IDMeal ASC
);

CREATE TABLE Rate_Restaurant
(
	IDRate_Restaurant    INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	IDHost_Restaurant    INTEGER NOT NULL,
	IDCustomer           INTEGER NULL,
	Rate                 INTEGER NOT NULL CHECK ( Rate BETWEEN 1 AND 5 )
)
 AUTO_INCREMENT = 1;

CREATE UNIQUE INDEX Unique_IDGuest_IDHost_Restaurant ON Rate_Restaurant
(
	IDCustomer ASC,
	IDHost_Restaurant ASC
);

CREATE TABLE Telephone
(
	IDTelephone          INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	Number               BIGINT	 NOT NULL,
	IDLocation           INTEGER NOT NULL
)
 AUTO_INCREMENT = 1;

CREATE TABLE User
(
	IDUser               INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	User_name            VARCHAR(20) NOT NULL,
	Password             VARCHAR(128) NOT NULL,
	E_mail               VARCHAR(50) NOT NULL
)
 AUTO_INCREMENT = 1;

CREATE UNIQUE INDEX Unique_username ON User
(
	User_name ASC
);

CREATE UNIQUE INDEX Unique_email ON User
(
	E_mail ASC
);

ALTER TABLE Administrator
ADD CONSTRAINT R_3 FOREIGN KEY (IDAdministrator) REFERENCES User (IDUser)
		ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Comment
ADD CONSTRAINT R_10 FOREIGN KEY (IDComment) REFERENCES Rate_Restaurant (IDRate_Restaurant)
		ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Customer
ADD CONSTRAINT R_1 FOREIGN KEY (IDCustomer) REFERENCES User (IDUser)
		ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Host_Restaurant
ADD CONSTRAINT R_2 FOREIGN KEY (IDHost_Restaurant) REFERENCES User (IDUser)
		ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Location
ADD CONSTRAINT R_16 FOREIGN KEY (IDHost_Restaurant) REFERENCES Host_Restaurant (IDHost_Restaurant)
		ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Meal
ADD CONSTRAINT R_15 FOREIGN KEY (IDHost_Restaurant) REFERENCES Host_Restaurant (IDHost_Restaurant)
		ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Rate_Meal
ADD CONSTRAINT R_20 FOREIGN KEY (IDCustomer) REFERENCES Customer (IDCustomer)
		ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE Rate_Meal
ADD CONSTRAINT R_21 FOREIGN KEY (IDMeal) REFERENCES Meal (IDMeal)
		ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Rate_Restaurant
ADD CONSTRAINT R_8 FOREIGN KEY (IDCustomer) REFERENCES Customer (IDCustomer)
		ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE Rate_Restaurant
ADD CONSTRAINT R_17 FOREIGN KEY (IDHost_Restaurant) REFERENCES Host_Restaurant (IDHost_Restaurant)
		ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Telephone
ADD CONSTRAINT R_19 FOREIGN KEY (IDLocation) REFERENCES Location (IDLocation)
		ON UPDATE CASCADE ON DELETE CASCADE;
		
DELIMITER $$	
CREATE TRIGGER trigger_rate_meal
BEFORE INSERT ON Rate_Meal
FOR EACH ROW
BEGIN
	DECLARE rate DECIMAL(2,1) DEFAULT 0;
	DECLARE newrate DECIMAL(2,1) DEFAULT 0;
	DECLARE num DECIMAL(2,1) DEFAULT 0;
	DECLARE idmeal INTEGER;
	
	SELECT NEW.IDMeal INTO idmeal;
	
	SELECT COUNT(*) INTO num
	FROM Rate_Meal
	WHERE Rate_Meal.IDMeal = idmeal
	GROUP BY Rate_Meal.IDMeal;
	
	SELECT Meal.Rate INTO rate
	FROM Meal
	WHERE Meal.IDMeal = idmeal;
	
	SELECT num*rate INTO rate;
	
	SELECT (NEW.Rate1+NEW.Rate2+NEW.Rate3+NEW.Rate4+NEW.Rate5)/5 INTO newrate;
	
	SELECT rate+newrate INTO rate;
	
	SELECT (num + 1) INTO num;
	
	SELECT rate/num INTO rate;
	
	UPDATE Meal
	SET Meal.Rate = rate
	WHERE Meal.IDMeal = idmeal;
	

END$$
DELIMITER ;	

DELIMITER $$	
CREATE TRIGGER trigger_rate_restaurant
BEFORE INSERT ON Rate_Restaurant
FOR EACH ROW
BEGIN
	DECLARE rate DECIMAL(2,1) DEFAULT 0;
	DECLARE newrate DECIMAL(2,1) DEFAULT 0;
	DECLARE num DECIMAL(2,1) DEFAULT 0;
	DECLARE idhost INTEGER;
	
	SELECT NEW.IDHost_Restaurant INTO idhost;
	
	SELECT COUNT(*) INTO num
	FROM Rate_Restaurant
	WHERE Rate_Restaurant.IDHost_Restaurant = idhost
	GROUP BY Rate_Restaurant.IDHost_Restaurant;
	
	SELECT Host_Restaurant.Rate INTO rate
	FROM Host_Restaurant
	WHERE Host_Restaurant.IDHost_Restaurant = idhost;
	
	SELECT num*rate INTO rate;
	
	SELECT NEW.Rate INTO newrate;
	
	SELECT rate+newrate INTO rate;
	
	SELECT (num + 1) INTO num;
	
	SELECT rate/num INTO rate;
	
	UPDATE Host_Restaurant
	SET Host_Restaurant.Rate = rate
	WHERE Host_Restaurant.IDHost_Restaurant = idhost;
	

END$$
DELIMITER ;

DELIMITER $$	
CREATE TRIGGER trigger_number_of_meals
BEFORE INSERT ON Meal
FOR EACH ROW
BEGIN
	DECLARE num INTEGER DEFAULT 0;
	DECLARE idhost INTEGER;
	
	SELECT NEW.IDHost_Restaurant INTO idhost;
	
	SELECT COUNT(*) INTO num
	FROM Meal
	WHERE Meal.IDHost_Restaurant = idhost
	GROUP BY Meal.IDHost_Restaurant;
	
	IF(num = 5)
	THEN
		SIGNAL sqlstate '45001';
	END IF;

END$$
DELIMITER ;

-- savakezic - sava123
-- fica - cofi
-- vex - moderator
-- kecoje - nemogu
-- pera - pera123

INSERT INTO User(User_name,Password,E_mail) VALUES ('savakezic','cdee838ec6db9514d5d7dc466ad43874ea2f4bba5bfec10301ba9b8e655506c7','sava.kezic@gmail.com');
INSERT INTO User(User_name,Password,E_mail) VALUES ('fica','de1bd3536bc54eb7145cfd1d4a1d9db6cf0f17b6b22aa3ef2e3cf20ce8e04dcf','tanic.filip.97@gmail.com');
INSERT INTO User(User_name,Password,E_mail) VALUES ('vex','ae2e5c961f8598f11fc72647383835f7f384bf1d07f7cf7000f5dcafaa0ee336','marko.vekaric30@gmail.com');
INSERT INTO User(User_name,Password,E_mail) VALUES ('kecoje','2ff9f70c92fd5811ac6e4d6f7b6f83c6bb956190991e25c90af53fd1a306ba03','jovan.kecojevic@gmail.com');
INSERT INTO User(User_name,Password,E_mail) VALUES ('pera','0f07ccc1d149aa468787ff5661c12490d9c2887509adca1341905a5eecf45a83','preazdera@gmail.com');

INSERT INTO Administrator(IDAdministrator, Name, Surname) VALUES(1, 'Sava','Kezic');

INSERT INTO Customer(IDCustomer,Name,Surname) VALUES (2,'Filip','Tanic');
INSERT INTO Customer(IDCustomer,Name,Surname) VALUES (4,'Jovan','Kecojevic');

INSERT INTO Host_Restaurant(IDHost_Restaurant, Description, Name) VALUES (3, 'Volim osmeh tvoj, bas lepo mi stoji.', 'Bife kod Vex-a');
INSERT INTO Host_Restaurant(IDHost_Restaurant, Description, Name) VALUES (5, 'Da se lepo nazderem', 'Kafana kod Pere');

INSERT INTO Meal(Name, Price, IDHost_Restaurant) VALUES ('Cinke', 100, 3);
INSERT INTO Meal(Name, Price, IDHost_Restaurant) VALUES ('Baklava', 120, 3);
INSERT INTO Meal(Name, Price, IDHost_Restaurant, Description) VALUES ('Cevapi', 300, 3, '10kom');
INSERT INTO Meal(Name, Price, IDHost_Restaurant, Description) VALUES ('Cevapi', 200, 3, '5kom');

INSERT INTO Meal(Name, Price, IDHost_Restaurant, Description) VALUES ('Cevapi', 330, 5, '10kom');
INSERT INTO Meal(Name, Price, IDHost_Restaurant, Description) VALUES ('Cevapi', 250, 5, '5kom');
INSERT INTO Meal(Name, Price, IDHost_Restaurant, Description) VALUES ('Tulumbe', 80, 5, 'kom');
INSERT INTO Meal(Name, Price, IDHost_Restaurant) VALUES ('Kajmak', 50, 5);

INSERT INTO Rate_Meal(IDCustomer, IDMeal, Rate1, Rate2, Rate3, Rate4, Rate5) VALUES (2,8,1,2,3,2,2);
INSERT INTO Rate_Meal(IDCustomer, IDMeal, Rate1, Rate2, Rate3, Rate4, Rate5) VALUES (4,8,3,2,5,1,3);
INSERT INTO Rate_Meal(IDCustomer, IDMeal, Rate1, Rate2, Rate3, Rate4, Rate5) VALUES (2,2,5,4,4,3,5);
INSERT INTO Rate_Meal(IDCustomer, IDMeal, Rate1, Rate2, Rate3, Rate4, Rate5) VALUES (4,2,4,4,5,5,3);

INSERT INTO Rate_Restaurant(IDCustomer, IDHost_Restaurant, Rate) VALUES (2,3,5);
INSERT INTO Rate_Restaurant(IDCustomer, IDHost_Restaurant, Rate) VALUES (4,3,4);
INSERT INTO Rate_Restaurant(IDCustomer, IDHost_Restaurant, Rate) VALUES (2,5,2);
INSERT INTO Rate_Restaurant(IDCustomer, IDHost_Restaurant, Rate) VALUES (4,5,1);

INSERT INTO Comment(Text,IDComment) VALUES('pa dobri ste',1);
INSERT INTO Comment(Text,IDComment) VALUES('katastrofa',2);
INSERT INTO Comment(Text,IDComment) VALUES('ne ulazite ovde',3);

INSERT INTO Location(Address, IDHost_Restaurant) VALUES ('Admirala Vukovica 43',3);
INSERT INTO Location(Address, IDHost_Restaurant) VALUES ('Nevesinska 2A',3);
INSERT INTO Location(Address, IDHost_Restaurant) VALUES ('Molerova 33',3);
INSERT INTO Location(Address, IDHost_Restaurant) VALUES ('Admirala Geparta 3',5);
INSERT INTO Location(Address, IDHost_Restaurant) VALUES ('Cika Ljubina 7B',5);

INSERT INTO Telephone(Number, IDLocation) VALUES ('381113975156',1);
INSERT INTO Telephone(Number, IDLocation) VALUES ('381114212452',1);
INSERT INTO Telephone(Number, IDLocation) VALUES ('381644212452',1);
INSERT INTO Telephone(Number, IDLocation) VALUES ('381651222342',2);
INSERT INTO Telephone(Number, IDLocation) VALUES ('381608788612',3);
INSERT INTO Telephone(Number, IDLocation) VALUES ('381623432756',4);
INSERT INTO Telephone(Number, IDLocation) VALUES ('381112453533',5);
INSERT INTO Telephone(Number, IDLocation) VALUES ('381622453533',5);


